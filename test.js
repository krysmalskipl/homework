var images = [
    "https://zaufanatrzeciastrona.pl/wp-content/uploads/2012/10/orange-150x150.jpg",
    "https://www.gardeningknowhow.com/wp-content/uploads/2015/03/pear-seeds-150x150.jpg",
    "http://static.wirtualnemedia.pl/media/150x110/apple_150.png"
];

var iloscZmian = [
	0,
	0,
	0
];

currentImage = 0;
isStop = false;

$(document).ready(function() {
    console.log("ready!");


});

function ChangeImage_Start() {
	if(isStop)
		return;
	$("#start").prop("disabled", true);
	$("#stop").prop("disabled", false);
	iloscZmian[0] = Math.floor((Math.random() * 40) + 1) + 20;
	iloscZmian[1] = Math.floor((Math.random() * 40) + 1) + 20;
	iloscZmian[2] = Math.floor((Math.random() * 40) + 1) + 20;
	ChangeImage();
}

function ChangeImage() {
        if (currentImage > images.length - 1)
            currentImage = 0;
		if(iloscZmian[0] != 0){
			$("#tryb1").slideDown(100, function() {
				$("#tryb1").attr("src", images[currentImage]);
				$("#tryb1").fadeIn(800);
			});
			if(isStop)
				iloscZmian[0]--;
		}
		if(iloscZmian[1] != 0){
			$("#tryb2").slideDown(100, function() {
				$("#tryb2").attr("src", images[currentImage]);
				$("#tryb2").fadeIn(800);
			});
			if(isStop)
				iloscZmian[1]--;
		}
		if(iloscZmian[2] != 0){
			$("#tryb3").slideDown(100, function() {
				$("#tryb3").attr("src", images[currentImage]);
				$("#tryb3").fadeIn(800);
			});
			if(isStop)
				iloscZmian[2]--;
		}
		if(iloscZmian[2] == 0 && iloscZmian[1] == 0 && iloscZmian[0] == 0){
			isStop = false;
			return;
		}
        currentImage++;
        setTimeout(function() {
            ChangeImage();
        }, 110);
}

$("#stop").click(function() {
    $("#stop").prop("disabled", true);
	$("#start").prop("disabled", false);
    console.log('stopped');
	isStop = true;

});